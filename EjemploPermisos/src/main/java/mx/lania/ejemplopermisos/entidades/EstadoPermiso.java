/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.ejemplopermisos.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author jaguilar
 */
@Entity
@Table(name = "estado_permiso")
public class EstadoPermiso implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_estado_permiso")
    private Integer idEstadoPermiso;
    @Column(name = "nombre")
    private String nombre;

    public EstadoPermiso() {
    }

    public EstadoPermiso(Integer idEstadoPermiso) {
        this.idEstadoPermiso = idEstadoPermiso;
    }

    public Integer getIdEstadoPermiso() {
        return idEstadoPermiso;
    }

    public void setIdEstadoPermiso(Integer idEstadoPermiso) {
        this.idEstadoPermiso = idEstadoPermiso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEstadoPermiso != null ? idEstadoPermiso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstadoPermiso)) {
            return false;
        }
        EstadoPermiso other = (EstadoPermiso) object;
        if ((this.idEstadoPermiso == null && other.idEstadoPermiso != null) || (this.idEstadoPermiso != null && !this.idEstadoPermiso.equals(other.idEstadoPermiso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.lania.ejemplopermisos.entidades.EstadoPermiso[ idEstadoPermiso=" + idEstadoPermiso + " ]";
    }
    
}
